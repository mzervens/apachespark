'''
Autors: Matīss Zērvēns
Izveidots: 06.03.2018

Uzdevuma formulējums:

Izmantojam Twitter troļļu datu kopu: 

https://www.nbcnews.com/tech/social-media/now-available-more-200-000-deleted-russian-troll-tweets-n844731
skat. CSV datnes ("Download tweets.csv (50 mb) and users.csv with full underlying data")
Uzdevums:

Veikt datu analīzi ar Apache Spark palīdzību
pirms tālākas datu apstrādes novērtējiet šo datu kvalitāti un, ja nepieciešams, veiciet datu attīrīšanu un kvalitātes uzlabošanu
Iegūt no datu kopas šādu informāciju:
tvītu un Twitter lietotāju tabulu ierakstu skaitu
agrāko un vēlāko (pēc datuma/laika) datos esošo Twitter ziņu
20 aktīvākos tvītu autorus un to tvītu skaitu, sakārtotus dilstošā tvītu skaita secībā
20 populārākos tvītos pieminētos hashtagus, sakārtotus dilstošā secībā pēc tvītu skaita, kur tie pieminēti
20 populārākos URL, kas pieminēti tvītos - te ir jāizmanto izvērstie URL, nevis t.co saīsinātie URL
tvītu skaitu pa mēnešiem
informāciju par katra mēneša 5 populārākajiem hashtagiem
Papildus izdomāt vēl 1-2  interesantas netriviālas atskaites, kas balstītas uz šiem datiem, un tās realizēt Spark-ā
šajā apakšuzdevumā varat iekļaut arī datu vizualizāciju

'''

import csv
import os 
import datetime
from operator import add

tweets = []
users = []

f = '%Y-%m-%d %H:%M:%S'
with open("../../tweets.csv") as csvDataFile:
    csvReader = csv.reader(csvDataFile)
    header = []
    for row in csvReader:
        header = row
        break 
    for row in csvReader:
        dict = {}
        for idx, val in enumerate(header):
            dict[val] = row[idx]
        tweets.append(dict)
with open("../../users.csv") as csvDataFile:
    csvReader = csv.reader(csvDataFile)
    header = []
    for row in csvReader:
        header = row
        break 
    for row in csvReader:
        dict = {}
        for idx, val in enumerate(header):
            dict[val] = row[idx]
        users.append(dict)

rdd_tweets = sc.parallelize(tweets)
rdd_users = sc.parallelize(users)


#Iegūt no datu kopas šādu informāciju:


#tvītu un Twitter lietotāju tabulu ierakstu skaitu

print("Tweetu skaits: " +  str(rdd_tweets.count()))
print("Lietotāju skaits: " +  str(rdd_users.count()))


#agrāko un vēlāko (pēc datuma/laika) datos esošo Twitter ziņu

first_tweet = rdd_tweets.filter(lambda x: x['created_str'] != '').map(lambda x: (x, datetime.datetime.strptime(x['created_str'], f))).min()
last_tweet = rdd_tweets.filter(lambda x: x['created_str'] != '').map(lambda x: (x, datetime.datetime.strptime(x['created_str'], f))).max()

#20 aktīvākos tvītu autorus un to tvītu skaitu, sakārtotus dilstošā tvītu skaita secībā

by_user_tweet_count = rdd_tweets.filter(lambda x: x['user_id'] != '').map(lambda x: (x['user_id'], 1)).reduceByKey(add).sortBy(lambda x: x[1], False).collect()
by_user_tweet_count20 = by_user_tweet_count[:20]
final_by_user_tweet_count20 = []

for stat in by_user_tweet_count20:
    user = rdd_users.filter(lambda x: x['id'] == stat[0]).collect()[0]
    final_by_user_tweet_count20.append({'username': user['screen_name'], 'tweet_count': stat[1]})


#20 populārākos tvītos pieminētos hashtagus, sakārtotus dilstošā secībā pēc tvītu skaita, kur tie pieminēti

tweet_hashtags = rdd_tweets.map(lambda x: x['hashtags'].replace('[', '').replace(']', '').split(",")).flatMap(lambda x: x).filter(lambda x: x != '')

tweet_hashtags = tweet_hashtags.map(lambda x: (x, 1)).reduceByKey(add).sortBy(lambda x: x[1], False).collect()

tweet_hashtags_res = tweet_hashtags[:20]

#20 populārākos URL, kas pieminēti tvītos - te ir jāizmanto izvērstie URL, nevis t.co saīsinātie URL
# analogi hashtags

tweet_urls = rdd_tweets.map(lambda x: x['expanded_urls'].replace('[', '').replace(']', '').split(",")).flatMap(lambda x: x).filter(lambda x: x != '').filter(lambda x: x != '""')

tweet_urls = tweet_urls.map(lambda x: (x, 1)).reduceByKey(add).sortBy(lambda x: x[1], False).collect()

tweet_urls_res = tweet_urls[:20]


#tvītu skaitu pa mēnešiem

tweets_month = rdd_tweets.filter(lambda x: x['created_str'] != '').map(lambda x: (datetime.datetime.strptime(x['created_str'], f).strftime("%Y-%m"), 1))

tweets_month = tweets_month.reduceByKey(add).sortBy(lambda x: x[1], False).collect()

#informāciju par katra mēneša 5 populārākajiem hashtagiem

for tw_m in tweets_month:
    month = tw_m[0]
    month_tweets = rdd_tweets.filter(lambda x: x['created_str'] != '').filter(lambda x: datetime.datetime.strptime(x['created_str'], f).strftime("%Y-%m") == month)
    # tālāk viss tas pats kas pie populārajiem tweetiem
    month_tweets = month_tweets.map(lambda x: x['hashtags'].replace('[', '').replace(']', '').split(",")).flatMap(lambda x: x).filter(lambda x: x != '')
    month_tweets = month_tweets.map(lambda x: (x, 1)).reduceByKey(add).sortBy(lambda x: x[1], False).collect()[:5]
    print("mēneša " + month + " populārākie hashtagi: ")
    print(month_tweets)
    break 
	